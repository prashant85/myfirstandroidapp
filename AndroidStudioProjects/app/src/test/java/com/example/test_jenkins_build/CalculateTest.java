package com.example.test_jenkins_build;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculateTest {

    @Test
    public void addMe () {

        Calculate calc = new Calculate ();
        long a = 20;
        long b =  40;
        long result = calc.AddMe (a,b);

        long expectVal = 60;

        assertEquals ( expectVal,result );
    }
}